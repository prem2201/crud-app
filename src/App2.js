import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import React from "react";
import Form from "./form2";
import Table from "./table2";

class Car extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      editdata: [],
    };
  }
  create = (data) => {
    if (!data.isEdit) {
      axios
        .post("https://61079f3ad73c6400170d3549.mockapi.io/movie/prem", {
          name: data.name,
          salary: data.salary,
          age: data.age,
        })
        .then((res) => {
          this.getAll();
        });
    } else {
      console.log(data.id);
      axios
        .put(
          `https://61079f3ad73c6400170d3549.mockapi.io/movie/prem/${data.id}`,
          {
            name: data.name,
            salary: data.salary,
            age: data.age,
          }
        )
        .then((res) => {
          this.getAll();
        });
    }
  };
  del = (data) => {
    var option = window.confirm(`are you want to delete ${data.name}`);
    if (option) {
      axios
        .delete(
          `https://61079f3ad73c6400170d3549.mockapi.io/movie/prem/${data.id}`,
          data
        )
        .then((res) => {
          console.log(res);
          this.getAll();
        });
    }
  };
  componentDidMount() {
    this.getAll();
  }
  getAll() {
    axios
      .get("https://61079f3ad73c6400170d3549.mockapi.io/movie/prem")
      .then((res) => {
        this.setState({ data: res.data });
      });
  }
  update = (data) => {
    this.setState({
      editdata: data,
    });
  };
  render() {
    return (
      <div style={{ padding: 20 }}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <center>
              <h1>CRUD APPLICATION</h1>
            </center>
          </Grid>
          <Grid item xs={12} md={4}>
            <Box boxShadow={1}>
              <div style={{ padding: 20 }}>
                <h1>ADD DETAILS</h1>
                <Form myData={this.create} setForm={this.state.editdata} />
              </div>
            </Box>
          </Grid>
          <Grid item xs={12} md={8}>
            <Box boxShadow={1}>
              <div style={{ padding: 20 }}>
                <Table
                  getDatas={this.state.data}
                  setData={this.update}
                  del={this.del}
                />
              </div>
            </Box>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default Car;
