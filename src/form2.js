import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import React from "react";
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      age: "",
      salary: "",
      isEdit: false,
    };
  }
  onChangename = (event) => {
    this.setState({ name: event.target.value });
  };
  onChangeage = (event) => {
    this.setState({ age: event.target.value });
  };
  onChangesal = (event) => {
    this.setState({ salary: event.target.value });
  };
  Submit = (event) => {
    event.preventDefault();
    if (!this.state.isEdit) {
      let data = {
        isEdit: this.state.isEdit,
        name: this.state.name,
        age: this.state.age,
        salary: this.state.salary,
      };
      this.props.myData(data);
    } else {
      let data = {
        isEdit: this.state.isEdit,
        id: this.state.id,
        name: this.state.name,
        age: this.state.age,
        salary: this.state.salary,
      };
      this.props.myData(data);
    }
  };

  componentWillReceiveProps(props) {
    if (props.setForm.id != null) {
      this.setState({
        isEdit: true,
        id: props.setForm.id,
        name: props.setForm.name,
        age: props.setForm.age,
        salary: props.setForm.salary,
      });
    }
  }

  render() {
    return (
      <div>
        <Grid container>
          <Grid item xs={12}>
            <form noValidate autoComplete="off" onSubmit={this.Submit}>
              <FormControl fullWidth>
                <InputLabel>name</InputLabel>
                <Input
                  color="primary"
                  onChange={this.onChangename}
                  name="Name"
                  value={this.state.name}
                />
              </FormControl>
              <br />
              <br />
              <FormControl fullWidth>
                <InputLabel>Age</InputLabel>
                <Input
                  color="primary"
                  onChange={this.onChangeage}
                  name="age"
                  value={this.state.age}
                />
              </FormControl>
              <br />
              <br />
              <FormControl fullWidth>
                <InputLabel>salary</InputLabel>
                <Input
                  color="primary"
                  onChange={this.onChangesal}
                  name="salary"
                  value={this.state.salary}
                />
              </FormControl>
              <br />
              <br />
              <center>
                <Button variant="contained" color="primary" type="submit">
                  {this.state.isEdit ? "Update" : "Create"}
                </Button>
              </center>
            </form>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default Form;
