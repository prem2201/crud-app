import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import React from "react";
class Car extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      age: "",
      salary: "",
    };
  }

  render() {
    return (
      <div>
        <TableContainer>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">
                  {" "}
                  <b>Name</b>
                </TableCell>
                <TableCell align="center">
                  <b>Age</b>
                </TableCell>
                <TableCell align="center">
                  <b>Salary</b>
                </TableCell>
                <TableCell align="center">
                  <b>Edit</b>
                </TableCell>
                <TableCell align="center">
                  <b>Delete</b>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.props.getDatas.map((e, index) => (
                <TableRow key={e.id}>
                  <TableCell align="center">{e.name}</TableCell>
                  <TableCell align="center">{e.age}</TableCell>
                  <TableCell align="center">{e.salary}</TableCell>
                  <TableCell align="center">
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={(event) => {
                        this.props.setData(e);
                      }}
                    >
                      Edit
                    </Button>
                  </TableCell>
                  <TableCell align="center">
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={(event) => {
                        this.props.del(e);
                      }}
                    >
                      delete
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    );
  }
}
export default Car;
